//
//  ViewController.swift
//  testeIoasys
//
//  Created by Gustavo Hassen on 09/03/19.
//  Copyright © 2019 Gustavo Hassen. All rights reserved.
//

import UIKit
extension UITextField {
 
    func setBottomBorder(){
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}
class ViewController: UIViewController {
    
    @IBOutlet weak var email: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        email.setBottomBorder()
    }


}

