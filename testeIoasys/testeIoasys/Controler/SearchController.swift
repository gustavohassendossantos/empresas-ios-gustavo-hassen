//
//  SearchController.swift
//  testeIoasys
//
//  Created by Gustavo Hassen on 14/03/19.
//  Copyright © 2019 Gustavo Hassen. All rights reserved.
//

import UIKit

class SearchController: UIViewController, UISearchBarDelegate {
    
    @IBOutlet weak var search: UIBarButtonItem!
    
    @IBAction func searchAction(sender: UIBarButtonItem) {
        // Create the search controller and specify that it should present its results in this same view
        let searchController = UISearchController(searchResultsController: nil)
        
        // Set any properties (in this case, don't hide the nav bar and don't show the emoji keyboard option)
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.keyboardType = UIKeyboardType.asciiCapable
        
        
        
        // Make this class the delegate and present the search
        searchController.searchBar.delegate = self
        if let navigationbar = self.navigationController?.navigationBar{
            navigationbar.barTintColor = UIColor.init(red: 222, green: 71, blue: 114, alpha: 1)
        }
        present(searchController, animated: true, completion: nil)
        
    }
}
